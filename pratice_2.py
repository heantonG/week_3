import turtle

t = turtle.Turtle()
my_color = ("red","midnightblue")
#t.width(1)
t.pensize(30)
t.speed(5)
for i in range(2):
    t.penup()
    t.goto(0,-i*75)
    t.pendown()
    t.color(my_color[i%len(my_color)])
    t.circle(10+i*75)

turtle.done()
