import random
import re


# parameters confirm 参数确认与说明
# repeat times  重复次数
re_times = 10000
# 清一色
m = 8# 清一色长度
real_count_qys = 0# 最终清一色数量
qys_record = []# 记录清一色的手牌
#---------------------------
count_qys = 0
#同花顺
ths_record = []# 记录同花顺的手牌
o = 5# 同花顺长度
real_count_ths = 0# 最终同花顺数量
poker_dict = {'A':1,'2':2,'3':3,'4':4,'5':5,'6':6,'7':7,'8':8,'9':9,'10':10,'J':11,'Q':12,'K':13}# 同花顺排序转换字典
#---------------------------
#---------------------------
#判断同花顺中的判断是否顺子函数
def ths2(z=[],o=5):
    sss = 0
    shunzi = 0
    if len(z) >= o:
        z_sort = sorted(z)
        for p in range(len(z)-1):
            if z_sort[p+1] == z_sort[p] + 1:#判断间隔为一由小到大的顺序
                shunzi += 1 
                # print(shunzi)
                if shunzi == o-1:
                    sss += 1
                    shunzi = 0
            else:
                shunzi = 0
                p -= 1
         
    return sss
#---------------------------
#判断同花顺函数
def ths(x=[],n=5):
    shunzi = 0
    ss = 0
    sz = []
    type = []
    for l in range(13):
        sep = re.split('(S|H|D|C)',x[l])
        sep.remove('')
        sz.append(poker_dict[sep[1]])
        type.append(sep[0])
    #按花色分类
    s = []
    h = []
    d = []
    c = []
    for q in range(13):
        if type[q] == 'H':
            h.append(sz[q])
        if type[q] == 'S':
            s.append(sz[q])
        if type[q] == 'D':
            d.append(sz[q])
        if type[q] == 'C':
            c.append(sz[q])
        #计算一副手牌不同花色的同花顺数量
        ss += ths2(h)
        ss += ths2(s)
        ss += ths2(d)
        ss += ths2(c)
    #返回本轮总共同花顺数量
    if ss >= 1:
        ths_record.append(x)
        ths_record.append('llllllll')
    return ss    
#---------------------------
#判断清一色的函数
def qys(x=[],n=5,Y='S'):
    type = []
    count_qys = 0
    for l in range(13):
        sep = re.split('(S|H|D|C)',x[l])
        sep.remove('')
        type.append(sep[0])
        if type[l] == Y:
            count_qys += 1
    # print(count_qys)

    if count_qys >= n:
        count_qys = 1
    else:count_qys = 0
    if count_qys == 1:
        qys_record.append(x)
        qys_record.append('llllllll')    
    return count_qys
    
#---------------------------
#---------------------------
# make a set of poker 搞一份新扑克
poker_type = ['S','H','D','C']
poker_card = ['A','2','3','4','5','6','7','8','9','10','J','Q','K']
cards =[]
for pt in poker_type:
    for pc in poker_card:
        cards.append(pt+pc)
#---------------------------
#---------------------------
#函数主体
for i in range(re_times):
    #make a new order of cards  打乱顺序
    t = 0
    new_cards=[]
    while t < 52:
        x = random.random()
        y = x * 52
        y = int(y)
        if cards[y] in new_cards:
            continue
        else:
            new_cards.append(cards[y])
            t += 1
    #distribute cards  派牌
    guest_a = new_cards[0:13]
    guest_b = new_cards[13:26]
    guest_c = new_cards[26:39]
    guest_d = new_cards[39:53]
    # 玩家A清一色判断
#     # print('A',guest_a)
    real_count_qys += qys(guest_a,m,'S')
    real_count_qys += qys(guest_a,m,'H')
    real_count_qys += qys(guest_a,m,'D')
    real_count_qys += qys(guest_a,m,'C')
#     # 玩家B清一色判断
#     # print('B',guest_b)
    real_count_qys += qys(guest_b,m,'S')
    real_count_qys += qys(guest_b,m,'H')
    real_count_qys += qys(guest_b,m,'D')
    real_count_qys += qys(guest_b,m,'C')
#     # 玩家C清一色判断
#     # print('C',guest_c)
    real_count_qys += qys(guest_c,m,'S')
    real_count_qys += qys(guest_c,m,'H')
    real_count_qys += qys(guest_c,m,'D')
    real_count_qys += qys(guest_c,m,'C')
#     # # 玩家D清一色判断
#     # print('D',guest_d)
    real_count_qys += qys(guest_d,m,'S')
    real_count_qys += qys(guest_d,m,'H')
    real_count_qys += qys(guest_d,m,'D')
    real_count_qys += qys(guest_d,m,'C')

#     # print('直到第',i,'轮,要求清',m,'一色数量是：',real_count_qys)
#     # print('-----------------------------')
#----------------------------------------------------------------------------------
    # 玩家A同花顺判断
    # print('A',guest_a)
    real_count_ths += ths(guest_a,o)
    # 玩家B同花顺判断
    # print('B',guest_b)
    real_count_ths += ths(guest_b,o)
    # 玩家C同花顺判断
    # print('C',guest_c)
    real_count_ths += ths(guest_a,o)
    # 玩家D同花顺判断
    # print('D',guest_d)
    real_count_ths += ths(guest_a,o)

    # print('直到第',i,'轮，',o,'张同花顺的数量是：',real_count_ths)
    # print('--------------------------------')

print('总共长度大于',m,'的清一色的数量是：',real_count_qys,'\n')
if real_count_qys >= 1:
    print('所以长度大于',m,'的清一色的概率是:',real_count_qys/re_times)
    print('分别是：',qys_record)
print('总共长度为',o,'的同花顺的数量是：',real_count_ths,'\n','所以长度为',o,'的同花顺的概率是：',real_count_ths/re_times)
if real_count_ths >= 1:
    print('所以长度为',o,'的同花顺的概率是：',real_count_ths/re_times)
    print('分别是：',ths_record)




                

    


    
        

